/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.entities;

import game.GamePanel;
import java.awt.*;
import java.awt.image.BufferedImage;
import static game.GamePanel.RES;

public class Enemy {

   // FIELDS
   private double x;
   private double y;
   private int r;

   private double dx;
   private double dy;
   private double rad;
   private double speed;

   private int health;
   private final int type;
   private final int rank;

   private boolean ready;
   private boolean dead;

   private boolean hit;
   private long hitTimer;

   private boolean slow;

   private BufferedImage image;
   private final BufferedImage imageFrames[] = new BufferedImage[4];
   
   private int frameCounter = 0;
   private int currentFrame = 0;

   // CONSTRUCTOR
   public Enemy(int type, int rank) {
      this.type = type;
      this.rank = rank;

      // default enemy
      if (type == 1) {
         // Gray enemy
         if (rank == 1) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_gray_10().getSubimage((i * 10), 0, 10, 10);
            }
            speed = 2;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 1;
         }
         if (rank == 2) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_gray_20().getSubimage((i * 20), 0, 20, 20);
            }
            speed = 2;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 2;
         }
         if (rank == 3) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_gray_30().getSubimage((i * 30), 0, 30, 30);
            }
            speed = 1.5;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 3;
         }
      }
      // stronger, faster default
      if (type == 2) {
         // Red enemy
         if (rank == 1) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_red_10().getSubimage((i * 10), 0, 10, 10);
            }
            speed = 3;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 2;
         }
         if (rank == 2) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_red_20().getSubimage((i * 20), 0, 20, 20);
            }
            speed = 3;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 3;
         }
         if (rank == 3) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_red_30().getSubimage((i * 30), 0, 30, 30);
            }
            speed = 2.5;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 3;
         }
      }
      // slow, but hard to kill
      if (type == 3) {
         // Green enemy
         if (rank == 1) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_green_10().getSubimage((i * 10), 0, 10, 10);
            }
            speed = 1.5;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 3;
         }
         if (rank == 2) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_green_20().getSubimage((i * 20), 0, 20, 20);
            }
            speed = 1.5;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 4;
         }
         if (rank == 3) {
            for (int i = 0; i < imageFrames.length; i++) {
               imageFrames[i] = RES.getEnemy_green_30().getSubimage((i * 30), 0, 30, 30);
            }
            speed = 1.5;
            image = imageFrames[0];
            r = image.getHeight() / 2;
            health = 5;
         }
      }

      x = Math.random() * GamePanel.PANEL_WIDTH / 2 + GamePanel.PANEL_WIDTH / 4;
      y = -r;

      double angle = Math.random() * 140 + 20;
      rad = Math.toRadians(angle);

      dx = Math.cos(rad) * speed;
      dy = Math.sin(rad) * speed;

      ready = false;
      dead = false;

      hit = false;
      hitTimer = 0;

   }

   // FUNCTIONS
   public double getx() {
      return x;
   }

   public double gety() {
      return y;
   }

   public int getr() {
      return r;
   }

   public int getType() {
      return type;
   }

   public int getRank() {
      return rank;
   }

   public void setSlow(boolean b) {
      slow = b;
   }

   public boolean isDead() {
      return dead;
   }

   public void hit() {
      health--;
      if (health <= 0) {
         dead = true;
      }
      hit = true;
      hitTimer = System.nanoTime();
   }

   public void explode() {

      if (rank > 1) {

         int amount = 0;
         if (type == 1) {
            amount = 3;
         }
         if (type == 2) {
            amount = 3;
         }
         if (type == 3) {
            amount = 4;
         }

         for (int i = 0; i < amount; i++) {

            Enemy e = new Enemy(getType(), getRank() - 1);
            e.setSlow(slow);
            e.x = this.x;
            e.y = this.y;
            double angle;
            if (!ready) {
               angle = Math.random() * 140 + 20;
            } else {
               angle = Math.random() * 360;
            }
            e.rad = Math.toRadians(angle);

            GamePanel.enemies.add(e);

         }

      }

   }

   public void update() {

      if (slow) {
         x += dx * 0.3;
         y += dy * 0.3;
      } else {
         x += dx;
         y += dy;
      }

      if (!ready) {
         if (x > r && x < GamePanel.PANEL_WIDTH - r
                 && y > r && y < GamePanel.PANEL_HEIGHT - r) {
            ready = true;
         }
      }

      if (x < r && dx < 0) {
         dx = -dx;
      }
      if (y < r && dy < 0) {
         dy = -dy;
      }
      if (x > GamePanel.PANEL_WIDTH - r && dx > 0) {
         dx = -dx;
      }
      if (y > GamePanel.PANEL_HEIGHT - r && dy > 0) {
         dy = -dy;
      }

      if (hit) {
         long elapsed = (System.nanoTime() - hitTimer) / 1000000;
         if (elapsed > 50) {
            hit = false;
            hitTimer = 0;
         }
      }

   }

   public void draw(Graphics2D g) {

      if (hit) {
//			g.setColor(Color.WHITE);
//			g.fillOval((int) (x - r), (int) (y - r), 2 * r, 2 * r);
//			
//			g.setStroke(new BasicStroke(3));
//			g.setColor(Color.WHITE.darker());
//			g.drawOval((int) (x - r), (int) (y - r), 2 * r, 2 * r);
//			g.setStroke(new BasicStroke(1));

         //g.drawImage(image, (int)x, (int)y, null);
         g.drawImage(getCurrentFrame(), (int) x, (int) y, 2 * r, 2 * r, Color.yellow, null);

      } else {
//			g.setColor(color1);
//			g.fillOval((int) (x - r), (int) (y - r), 2 * r, 2 * r);
//			
//			g.setStroke(new BasicStroke(3));
//			g.setColor(color1.darker());
//			g.drawOval((int) (x - r), (int) (y - r), 2 * r, 2 * r);
//			g.setStroke(new BasicStroke(1));

         g.drawImage(getCurrentFrame(), (int) x, (int) y, null);

      }

   }

   /**
    * @return the imageFrames
    */
   public BufferedImage[] getImageFrames() {
      return imageFrames;
   }

   /**
    * @return the image
    */
   public BufferedImage getImage() {
      return image;
   }

   /**
    * @param image the image to set
    */
   public void setImage(BufferedImage image) {
      this.image = image;
   }

   public BufferedImage getCurrentFrame() {
        if ((frameCounter++) % 2 == 0) {
            return imageFrames[currentFrame++ % 4];
        } else {
            return imageFrames[currentFrame % 4];
        }
    }
   
}
