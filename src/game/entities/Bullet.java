/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.entities;

import game.GamePanel;
import static game.GamePanel.RES;
import java.awt.*;
import java.io.IOException;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Bullet {

   // FIELDS
   private double x;
   private double y;
   private final int r;

   private final double dx;
   private final double dy;
   private final double rad;
   private final double speed;

   private final Color color1;

   private Clip clip;

   // CONSTRUCTOR
   public Bullet(double angle, int x, int y) {

      this.x = x;
      this.y = y;
      r = 2;

      rad = Math.toRadians(angle);
      speed = 10;
      dx = Math.cos(rad) * speed;
      dy = Math.sin(rad) * speed;

      color1 = Color.YELLOW;
      try {
         clip = AudioSystem.getClip();
         clip.open(AudioSystem.getAudioInputStream(RES.shotSoundURL));
         clip.start();
      } catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
         e.printStackTrace();
      } finally {
         clip.drain();
      }
   }

   // FUNCTIONS
   public double getx() {
      return x;
   }

   public double gety() {
      return y;
   }

   public double getr() {
      return r;
   }

   public boolean update() {
      x += dx;
      y += dy;
      return x < -r || x > GamePanel.PANEL_WIDTH + r
              || y < -r || y > GamePanel.PANEL_HEIGHT + r;

   }

   public void draw(Graphics2D g) {

      g.setColor(color1);
      g.fillOval((int) (x - r), (int) (y - r), 2 * r, 2 * r);

   }

}
