/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package game;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;

/**
 *
 * @author Łukasz Gawron <gawronlucas@gmail.com>
 */
public class Game {

   
   public static void main(String[] args) {
      try {
         JFrame window = new JFrame("Galaxy Destruction");
         window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         //window.setContentPane(new GamePanel());
         window.add(new GamePanel());
         window.pack();
         window.setVisible(true);
      } catch (IOException | LineUnavailableException | UnsupportedAudioFileException ex) {
         Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
      }
   }
   
}
