/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package res.classes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Łukasz Gawron <gawronlucas@gmail.com>
 */
public final class Resources {

   public final URL shotSoundURL = getClass().getResource("/res/shot.wav");
   public final URL backgroundSoundURL = getClass().getResource("/res/imperial_march.wav");;

   private final URL backgroundURL = getClass().getResource("/res/bg_space.gif");
   private final URL enemy_gray_10URL = getClass().getResource("/res/sp_gray_10.png");
   private final URL enemy_red_10URL = getClass().getResource("/res/sp_red_10.png");
   private final URL enemy_green_10URL = getClass().getResource("/res/sp_green_10.png");
   private final URL enemy_gray_20URL = getClass().getResource("/res/sp_gray_20.png");
   private final URL enemy_red_20URL = getClass().getResource("/res/sp_red_20.png");
   private final URL enemy_green_20URL = getClass().getResource("/res/sp_green_20.png");
   private final URL enemy_gray_30URL = getClass().getResource("/res/sp_gray_30.png");
   private final URL enemy_red_30URL = getClass().getResource("/res/sp_red_30.png");
   private final URL enemy_green_30URL = getClass().getResource("/res/sp_green_30.png");

   private File shotSound;
   private File backgroundSound;

   private BufferedImage background;
   private BufferedImage enemy_gray_10;
   private BufferedImage enemy_red_10;
   private BufferedImage enemy_green_10;
   private BufferedImage enemy_gray_20;
   private BufferedImage enemy_red_20;
   private BufferedImage enemy_green_20;
   private BufferedImage enemy_gray_30;
   private BufferedImage enemy_red_30;
   private BufferedImage enemy_green_30;

   public Resources() {
      try {
         setShotSound(new File("/res/shot.wav"));
         setBackgroundSound(new File("/res/imperial_march.wav"));

         setBackground(ImageIO.read(getBackgroundURL()));
         setEnemy_gray_10(ImageIO.read(getEnemy_gray_10URL()));
         setEnemy_gray_20(ImageIO.read(getEnemy_gray_20URL()));
         setEnemy_gray_30(ImageIO.read(getEnemy_gray_30URL()));

         setEnemy_red_10(ImageIO.read(getEnemy_red_10URL()));
         setEnemy_red_20(ImageIO.read(getEnemy_red_20URL()));
         setEnemy_red_30(ImageIO.read(getEnemy_red_30URL()));

         setEnemy_green_10(ImageIO.read(getEnemy_green_10URL()));
         setEnemy_green_20(ImageIO.read(getEnemy_green_20URL()));
         setEnemy_green_30(ImageIO.read(getEnemy_green_30URL()));
      } catch (IOException ex) {
         Logger.getLogger(Resources.class.getName()).log(Level.SEVERE, null, ex);
      }
   }

   /**
    * @return the backgroundURL
    */
   private URL getBackgroundURL() {
      return backgroundURL;
   }

   /**
    * @return the enemy_gray_10URL
    */
   private URL getEnemy_gray_10URL() {
      return enemy_gray_10URL;
   }

   /**
    * @return the enemy_red_10URL
    */
   private URL getEnemy_red_10URL() {
      return enemy_red_10URL;
   }

   /**
    * @return the enemy_green_10URL
    */
   private URL getEnemy_green_10URL() {
      return enemy_green_10URL;
   }

   /**
    * @return the enemy_gray_20URL
    */
   private URL getEnemy_gray_20URL() {
      return enemy_gray_20URL;
   }

   /**
    * @return the enemy_red_20URL
    */
   private URL getEnemy_red_20URL() {
      return enemy_red_20URL;
   }

   /**
    * @return the enemy_green_20URL
    */
   private URL getEnemy_green_20URL() {
      return enemy_green_20URL;
   }

   /**
    * @return the enemy_gray_30URL
    */
   private URL getEnemy_gray_30URL() {
      return enemy_gray_30URL;
   }

   /**
    * @return the enemy_red_30URL
    */
   private URL getEnemy_red_30URL() {
      return enemy_red_30URL;
   }

   /**
    * @return the enemy_green_30URL
    */
   private URL getEnemy_green_30URL() {
      return enemy_green_30URL;
   }

   /**
    * @return the background
    */
   public BufferedImage getBackground() {
      return background;
   }

   /**
    * @return the enemy_gray_10
    */
   public BufferedImage getEnemy_gray_10() {
      return enemy_gray_10;
   }

   /**
    * @return the enemy_red_10
    */
   public BufferedImage getEnemy_red_10() {
      return enemy_red_10;
   }

   /**
    * @return the enemy_green_10
    */
   public BufferedImage getEnemy_green_10() {
      return enemy_green_10;
   }

   /**
    * @return the enemy_gray_20
    */
   public BufferedImage getEnemy_gray_20() {
      return enemy_gray_20;
   }

   /**
    * @return the enemy_red_20
    */
   public BufferedImage getEnemy_red_20() {
      return enemy_red_20;
   }

   /**
    * @return the enemy_green_20
    */
   public BufferedImage getEnemy_green_20() {
      return enemy_green_20;
   }

   /**
    * @return the enemy_gray_30
    */
   public BufferedImage getEnemy_gray_30() {
      return enemy_gray_30;
   }

   /**
    * @return the enemy_red_30
    */
   public BufferedImage getEnemy_red_30() {
      return enemy_red_30;
   }

   /**
    * @return the enemy_green_30
    */
   public BufferedImage getEnemy_green_30() {
      return enemy_green_30;
   }

   /**
    * @param background the background to set
    */
   public void setBackground(BufferedImage background) {
      this.background = background;
   }

   /**
    * @param enemy_gray_10 the enemy_gray_10 to set
    */
   public void setEnemy_gray_10(BufferedImage enemy_gray_10) {
      this.enemy_gray_10 = enemy_gray_10;
   }

   /**
    * @param enemy_red_10 the enemy_red_10 to set
    */
   public void setEnemy_red_10(BufferedImage enemy_red_10) {
      this.enemy_red_10 = enemy_red_10;
   }

   /**
    * @param enemy_green_10 the enemy_green_10 to set
    */
   public void setEnemy_green_10(BufferedImage enemy_green_10) {
      this.enemy_green_10 = enemy_green_10;
   }

   /**
    * @param enemy_gray_20 the enemy_gray_20 to set
    */
   public void setEnemy_gray_20(BufferedImage enemy_gray_20) {
      this.enemy_gray_20 = enemy_gray_20;
   }

   /**
    * @param enemy_red_20 the enemy_red_20 to set
    */
   public void setEnemy_red_20(BufferedImage enemy_red_20) {
      this.enemy_red_20 = enemy_red_20;
   }

   /**
    * @param enemy_green_20 the enemy_green_20 to set
    */
   public void setEnemy_green_20(BufferedImage enemy_green_20) {
      this.enemy_green_20 = enemy_green_20;
   }

   /**
    * @param enemy_gray_30 the enemy_gray_30 to set
    */
   public void setEnemy_gray_30(BufferedImage enemy_gray_30) {
      this.enemy_gray_30 = enemy_gray_30;
   }

   /**
    * @param enemy_red_30 the enemy_red_30 to set
    */
   public void setEnemy_red_30(BufferedImage enemy_red_30) {
      this.enemy_red_30 = enemy_red_30;
   }

   /**
    * @param enemy_green_30 the enemy_green_30 to set
    */
   public void setEnemy_green_30(BufferedImage enemy_green_30) {
      this.enemy_green_30 = enemy_green_30;
   }

   /**
    * @return the shotSound
    */
   public File getShotSound() {
      return shotSound;
   }

   /**
    * @param shotSound the shotSound to set
    */
   public void setShotSound(File shotSound) {
      this.shotSound = shotSound;
   }

   /**
    * @return the backgroundSound
    */
   public File getBackgroundSound() {
      return backgroundSound;
   }

   /**
    * @param backgroundSound the backgroundSound to set
    */
   public void setBackgroundSound(File backgroundSound) {
      this.backgroundSound = backgroundSound;
   }

}
